{
  description = "PandocProject";
  inputs.haskellNix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskellNix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils, haskellNix }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
    let
      overlays = [ haskellNix.overlay
        (final: prev: {
          # This overlay adds our project to pkgs
          pandocProject = final.haskell-nix.cabalProject {
              cabalProjectFreeze = null;
              cabalProject = null;
              cabalProjectLocal = null;

              compiler-nix-name = "ghc8104";
              name = "pandocProject";

              src = final.fetchFromGitHub {
                 name = "pandoc";
                 owner = "jgm";
                 repo = "pandoc";
                 rev = "d05460d00d7c9af3b4913f1760ea385a7d855d84";
                 sha256 = "1a3kwag6j13b42zhzxiwlzabsc6c9jppiwv9j8gbnf2k1yb84kdm";
              };

              pkg-def-extras = with final.haskell.lib; [];
            };
        })
      ];
      pkgs = import nixpkgs { inherit system overlays; };
      flake = pkgs.pandocProject.flake {};
    in flake // {
      # Built by `nix build .`
      inherit overlays;
      defaultPackage = flake.packages."pandoc:exe:pandoc";
    });
}
